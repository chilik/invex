﻿using Core.Constants;
using Core.Services.External;
using Infrastructure.Entities;

namespace Infrastructure.Seeder
{
    public static class IdentitySeeder
    {
        public static void SeedRoles(AppRoleManager roleManager)
        {
            if (!roleManager.RoleExistsAsync(Roles.USER).Result)
            {
                var role = new Role
                {
                    Name = Roles.USER
                };
                _ = roleManager.CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync(Roles.ADMIN).Result)
            {
                var role = new Role
                {
                    Name = Roles.ADMIN
                };
                _ = roleManager.CreateAsync(role).Result;
            }
        }

        public static void SeedUser(AppUserManager userManager, string email, string login, string password, string role)
        {
            if (userManager.FindByNameAsync(login).Result == null && userManager.FindByEmailAsync(email).Result == null)
            {
                var user = new User { Email = email, UserName = login };
                _ = userManager.CreateAsync(user, password).Result;
                _ = userManager.AddToRoleAsync(user, role).Result;
            }
        }
    }
}
