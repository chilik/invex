﻿using AutoMapper;
using Core.Exceptions;
using Core.Models;
using Core.Services.External;
using Core.Services.Interfaces;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Services.Implementations
{
    public class WorkspaceService : IWorkspaceService
    {
        private readonly AppIdentityDbContext _context;
        private readonly IMapper _mapper;
        private readonly AppUserManager _userManager;

        public WorkspaceService(
            AppIdentityDbContext context,
            IMapper mapper,
            AppUserManager userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<Workspace.Response> Create(Workspace.Request model)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user == null) throw new CoreException("User not found");

            var workspace = _mapper.Map<Infrastructure.Entities.Workspace>(model);
            workspace.Id = Guid.NewGuid();
            workspace.Users = new List<Infrastructure.Entities.User> { user };
            workspace.OwnerId = user.Id;

            await _context.Workspaces.AddAsync(workspace);
            await _context.SaveChangesAsync();

            return _mapper.Map<Workspace.Response>(workspace);
            
        }

        public async Task Delete(Guid id)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user == null) throw new CoreException("User not found");

            var workspace = user.Workspaces.FirstOrDefault(x => x.Id == id);
            if (workspace == null || workspace.OwnerId != user.Id) throw new CoreException("Workspace not found");

            _context.Workspaces.Remove(workspace);
            _context.SaveChanges();
        }

        public async Task<IEnumerable<Workspace.Response>> GetAll()
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user == null) throw new CoreException("User not found");

            return _mapper.Map<IEnumerable<Workspace.Response>>(user.Workspaces);
        }

        public async Task<Workspace.Response> GetById(Guid id)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user == null) throw new CoreException("User not found");

            return _mapper.Map<Workspace.Response>(user.Workspaces.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Workspace.Response> Update(Workspace.Request model)
        {
            var user = await _userManager.GetCurrentUserAsync();
            if (user == null) throw new CoreException("User not found");

            var workspace = user.Workspaces.FirstOrDefault(x => x.Id == model.Id);
            if (workspace == null || workspace.OwnerId != user.Id) throw new CoreException("Workspace not found");

            workspace = _mapper.Map(model, workspace);
            _context.Workspaces.Update(workspace);
            _context.SaveChanges();

            return _mapper.Map<Workspace.Response>(workspace);
        }

        public async Task<string> GenerateInviteCode(Guid id)
        {
            return null;
        }

        public Task<Workspace.Response> Join(string inviteCode)
        {
            throw new NotImplementedException();
        }

        public Task<Workspace.Response> Kick(Guid id, string userId)
        {
            throw new NotImplementedException();
        }

        public Task Leave(Guid id)
        {
            throw new NotImplementedException();
        }
        
    }
}
