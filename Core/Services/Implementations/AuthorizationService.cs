﻿using AutoMapper;
using Core.Constants;
using Core.Exceptions;
using Core.Models;
using Core.Services.External;
using Core.Services.Interfaces;
using Infrastructure.Entities;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Core.Services.Implementations
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly AppUserManager _userManager;
        private readonly IMapper _mapper;
        private readonly AppIdentityDbContext _context;
        private readonly ITokenService _tokenService;

        public AuthorizationService(AppUserManager userManager, IMapper mapper, AppIdentityDbContext context, ITokenService tokenService)
        {
            _userManager = userManager;
            _tokenService = tokenService;
            _mapper = mapper;
            _context = context;
        }

        public async Task<SignIn.Response> SignIn(SignIn.Request model)
        {
            var user = await _userManager.FindByLoginAsync(model.Login);
            if (user == null)
                throw new CoreException("Invalid login or password");
            var success = await _userManager.CheckPasswordAsync(user, model.Password);
            if (!success) 
                throw new CoreException("Invalid login or password");

            return new SignIn.Response
            {
                Token = new Token
                {
                    Access = await _tokenService.GenerateAccessTokenAsync(user.UserName),
                    Refresh = await _tokenService.GenerateRefreshTokenAsync(user.UserName, model.Fingerprint)
                },
                Id = user.Id
            };
        }

        public async Task<SignUp.Response> SignUp(SignUp.Request model)
        {
            if (await _userManager.FindByNameAsync(model.Login) != null) throw new CoreException("Login has already been taked");
            if (await _userManager.FindByEmailAsync(model.Email) != null) throw new CoreException("User with that email already registered");
            var user = _mapper.Map<User>(model);
            await _userManager.CreateAsync(user, model.Password);
            await _userManager.AddToRoleAsync(user, Roles.USER);
            return _mapper.Map<SignUp.Response>(user);
        }

        public async Task<SignIn.Response> Refresh(Models.Refresh.Request model)
        {
            var token = await _context.RefreshTokens.Include(x=>x.User).FirstOrDefaultAsync(x => x.Token == model.Token && x.Fingerprint == model.Fingerprint);
            if (token == null || token.Expiration < DateTimeOffset.Now) throw new CoreException("Invalid refresh token");

            return new SignIn.Response {
                Token = new Token {
                    Access = await _tokenService.GenerateAccessTokenAsync(token.User.UserName),
                    Refresh = await _tokenService.GenerateRefreshTokenAsync(token.User.UserName, model.Fingerprint)
                }, 
                Id = token.User.Id 
            };
        }

    }
}
