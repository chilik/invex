﻿using AutoMapper;
using Core.Exceptions;
using Core.Models;
using Core.Services.Interfaces;
using Infrastructure.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Services.Implementations
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        public ProfileService(UserManager<User> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<UserProfile.Response> GetById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) throw new CoreException("Profile not found");
            return _mapper.Map<UserProfile.Response>(user);
        }
    }
}
