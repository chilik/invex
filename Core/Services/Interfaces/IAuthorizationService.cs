﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Services.Interfaces
{
    public interface IAuthorizationService
    {
        public Task<SignUp.Response> SignUp(SignUp.Request model);
        public Task<SignIn.Response> SignIn(SignIn.Request model);
        public Task<SignIn.Response> Refresh(Refresh.Request model);
    }
}
