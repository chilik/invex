﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services.Interfaces
{
    public interface IWorkspaceService
    {
        public Task<IEnumerable<Workspace.Response>> GetAll();
        public Task<Workspace.Response> GetById(Guid id);
        public Task<Workspace.Response> Create(Workspace.Request model);
        public Task<Workspace.Response> Update(Workspace.Request model);
        public Task Delete(Guid id);
        public Task<string> GenerateInviteCode(Guid id);
        public Task Leave(Guid id);
        public Task<Workspace.Response> Join(string inviteToken);
        public Task<Workspace.Response> Kick(Guid id, string userId);

    }
}
