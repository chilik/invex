﻿using Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services.Interfaces
{
    public interface ITokenService
    {
        public Task<string> GenerateAccessTokenAsync(string userName);
        public Task<string> GenerateRefreshTokenAsync(string userName, string fingerprint);
        public Task<string> GenerateInviteTokenAsync(Workspace workspace);
        public Task<bool> ValidateInviteTokenAsync(string token);
    }
}
