﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Services.Interfaces
{
    public interface IProfileService
    {
        public Task<UserProfile.Response> GetById(string id);
    }
}
