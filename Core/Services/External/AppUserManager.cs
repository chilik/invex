﻿using Infrastructure.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Services.External
{
    public class AppUserManager : UserManager<User>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AppUserManager(
            IHttpContextAccessor httpContextAccessor,
            IUserStore<User> store, 
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<User> passwordHasher, 
            IEnumerable<IUserValidator<User>> userValidators, 
            IEnumerable<IPasswordValidator<User>> passwordValidators, 
            ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<User>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        
        public Task<User> FindByLoginAsync(string login)
        {
            return Users.FirstOrDefaultAsync(x => x.UserName == login || x.Email == login);
        }

        public Task<User> GetCurrentUserAsync()
        {
            var userId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            return userId != null ? Users.Include(x => x.Workspaces).FirstOrDefaultAsync(x => x.Id == userId) : null;
        }

    }
}
