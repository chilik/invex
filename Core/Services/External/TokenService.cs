﻿using Core.Constants;
using Core.Exceptions;
using Core.Services.Interfaces;
using Infrastructure.Entities;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services.External
{
    public class TokenService : ITokenService
    {
        private readonly AppUserManager _userManager;
        private readonly AppIdentityDbContext _context;
        
        public TokenService(AppUserManager userManager, AppIdentityDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<string> GenerateAccessTokenAsync(string userName)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Settings.JWT_SECRET_KEY);
            var user = await _userManager.FindByNameAsync(userName);
            var roles = await _userManager.GetRolesAsync(user);
            var claims = new List<Claim> { new Claim("id", user.Id), new Claim(ClaimTypes.Name, userName) };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.ToArray()),
                Expires = DateTime.UtcNow.AddMinutes(Settings.ACCESS_LIFETIME),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<string> GenerateRefreshTokenAsync(string userName, string fingerprint)
        {
            var user = await _userManager.FindByNameAsync(userName);
            var randomNumber = new byte[32];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            var refresh = Convert.ToBase64String(randomNumber);
            var token = await _context.RefreshTokens.FirstOrDefaultAsync(x => x.Fingerprint == fingerprint && x.UserId == user.Id);
            if (token == null)
            {
                token = new Infrastructure.Entities.Refresh
                {
                    Id = Guid.NewGuid(),
                    Expiration = DateTimeOffset.Now.AddMinutes(Settings.REFRESH_LIFETIME),
                    UserId = user.Id,
                    Fingerprint = fingerprint,
                    Token = refresh
                };
                _context.RefreshTokens.Add(token);
            }
            else
            {
                token.Expiration = DateTimeOffset.Now.AddMinutes(Settings.REFRESH_LIFETIME);
                token.Token = refresh;
                _context.RefreshTokens.Update(token);
            }
            _context.SaveChanges();
            return refresh;
        }

        public async Task<string> GenerateInviteTokenAsync(Workspace workspace)
        {
            if (workspace == null) throw new CoreException("Workspace not found");
            var key = workspace.Id.ToByteArray();
            return Convert.ToBase64String(key);
        }

        public async Task<bool> ValidateInviteTokenAsync(string token)
        {
            var key = Convert.FromBase64String(token);
            var id = new Guid(key);
            var workspace = await _context.Workspaces.FirstOrDefaultAsync(x => x.Id == id);
            return workspace == null;
        }
    }
}
