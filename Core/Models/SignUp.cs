﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SignUp
    {
        public class Request
        {
            public string Email { get; set; }
            public string Login { get; set; }
            public string Password { get; set; }

            [JsonPropertyName("confirmation-password")]
            public string ConfirmationPassword { get; set; }
        }

        public class Response
        {
            public string Id { get; set; }
            public string Login { get; set; }
            public string Email { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(x => x.Login).NotEmpty().WithMessage("Login required");
                RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Email required");
                RuleFor(x => x.Password)
                    .MinimumLength(6).WithMessage("Minimum length: 6.")
                    .MaximumLength(32).WithMessage("Maximum length: 32.")
                    .Matches(@"\d").WithMessage("Must include at least one alphanumeric character.")
                    .Matches(@"[A-Z]").WithMessage("Must include at least one uppercase letter.")
                    .Equal(c => c.ConfirmationPassword).WithMessage("Password mismatch");
            }
        }
    }
}
