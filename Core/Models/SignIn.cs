﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SignIn
    {
        public class Request
        {
            public string Login { get; set; }
            public string Password { get; set; }
            public string Fingerprint { get; set; }

        }

        public class Response
        {
            public string Id { get; set; }
            public Token Token { get; set; }
        }

        public class RequestValidation : AbstractValidator<Request>
        {
            public RequestValidation()
            {
                RuleFor(x => x.Login).NotEmpty().WithMessage("Login required.");
                RuleFor(x => x.Password).NotEmpty().WithMessage("Password required.");
            }
        }
    }
}
