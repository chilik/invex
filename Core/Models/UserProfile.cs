﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class UserProfile
    {
        public class Response
        {
            public string Id { get; set; }
            public string Login { get; set; }
            public string Email { get; set; }
        }

        public class Request
        {
            public string Login { get; set; }
            public string Email { get; set; }
        }
    }
}
