﻿using Core.Constants;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Error
    {
        public string Type { get; set; } = ErrorTypes.SERVER_ERROR;
        public string Status { get; set; } = "Error";
        public int Code { get; set; } = StatusCodes.Status400BadRequest;
        public string Message { get; set; }
        public Dictionary<string, string[]> Errors { get; set; }
    }
}
