﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Workspace
    {
        public class Request
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class Response
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string OwnerId { get; set; }
            public string InviteToken { get; set; }
            public IEnumerable<UserProfile.Response> Users { get; set; }
        }

        public class RequestValidator : AbstractValidator<Request>
        {
            public RequestValidator()
            {
                RuleFor(x => x.Name).NotEmpty().WithMessage("Name required")
                    .MinimumLength(6).WithMessage("Minimum length: 6.")
                    .MaximumLength(32).WithMessage("Maximum length: 32.");
            }
        }
    }
}
