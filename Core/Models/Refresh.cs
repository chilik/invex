﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Refresh
    {
        public class Request
        {
            public string Token { get; set; }
            public string Fingerprint { get; set; }
        }
    }
}
