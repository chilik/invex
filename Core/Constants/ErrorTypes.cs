﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Constants
{
    public static class ErrorTypes
    {
        public const string SERVER_ERROR = "Server error";
        public const string VALIDATION_ERROR = "https://tools.ietf.org/html/rfc7231#section-6.5.1";
    }
}
