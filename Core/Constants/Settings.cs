﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Constants
{
    public static class Settings
    {
        public const string AUTH_KEY = "AuthKeyOfDoomThatMustBeAMinimumNumberOfBytes";

        public const string JWT_SECRET_KEY = "wfHuc2fXABpWD6v2rgPqhmPXk5Mswyn5";

        public const string ISSUER = "localhost";

        public const string AUDIENCE = "localhost";

        public const int ACCESS_LIFETIME = 60;

        public const int REFRESH_LIFETIME = 1440;
    }
}
