﻿using AutoMapper;
using Core.Models;
using Infrastructure.Entities;

namespace Invex.Data.Config.AutomapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<SignUp.Request, User>().ForMember(
                dest => dest.UserName,
                opt => opt.MapFrom(src => src.Login)
            );

            CreateMap<User, SignUp.Response>().ForMember(
                dest => dest.Login,
                opt => opt.MapFrom(src => src.UserName)
            );

            CreateMap<Core.Models.UserProfile.Request, User>().ForMember(
                dest => dest.UserName,
                opt => opt.MapFrom(src => src.Login)
            );

            CreateMap<User, Core.Models.UserProfile.Response>().ForMember(
                    dest=>dest.Login,
                    opt => opt.MapFrom(src => src.UserName)
            );
        }
    }
}
