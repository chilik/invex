﻿using AutoMapper;
using Infrastructure.Entities;

namespace Invex.Data.Config.AutomapperProfiles
{
    public class WorkspaceProfile : Profile
    {
        public WorkspaceProfile()
        {
            CreateMap<Core.Models.Workspace.Request, Workspace>();

            CreateMap<Workspace, Core.Models.Workspace.Response>();
        }
    }
}
