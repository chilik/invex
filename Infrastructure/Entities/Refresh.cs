﻿using System;

namespace Infrastructure.Entities
{
    public class Refresh
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Token { get; set; }
        public DateTimeOffset Expiration { get; set; }
        public string Fingerprint { get; set; }
    }
}
