﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Entities
{
    public class User : IdentityUser
    {
        public string Refresh { get; set; }
        public DateTimeOffset Expires { get; set; }
        public virtual IList<Workspace> Workspaces { get; set; }
        public virtual IList<Refresh> RefreshTokens { get; set; }
    }
}
