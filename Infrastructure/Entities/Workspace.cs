﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Entities
{
    public class Workspace
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string InviteToken { get; set; }
        public string OwnerId { get; set; }
        public virtual IList<User> Users { get; set; }
    }
}
