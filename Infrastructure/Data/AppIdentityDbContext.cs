﻿using Infrastructure.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Identity
{
    public class AppIdentityDbContext : IdentityDbContext<User, Role, string>
    {
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> opt) : base(opt)
        {
        }

        public DbSet<Workspace> Workspaces { get; set; }
        public DbSet<Refresh> RefreshTokens { get; set; }
    }
}
