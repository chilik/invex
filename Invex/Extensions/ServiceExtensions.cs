﻿using AutoMapper;
using Core.Constants;
using Core.Models;
using Core.Services.External;
using FluentValidation.AspNetCore;
using Infrastructure.Entities;
using Infrastructure.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Text;

namespace Invex.Extensions
{
    public static class ServiceExtensions
    {
        public static void AddAppDbContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<AppIdentityDbContext>(opt =>
            {
                opt.UseNpgsql(config.GetConnectionString("DefaultConnection"), opt => opt.MigrationsAssembly("Invex"));
            });
        }
        public static void AddAppIdentity(this IServiceCollection services)
        {
            services.AddIdentity<User, Role>(opt =>
            {
                opt.Password.RequireDigit = true;
                opt.Password.RequiredLength = 6;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequiredUniqueChars = 0;
                opt.Password.RequireUppercase = true;

            })
            .AddUserManager<AppUserManager>()
            .AddRoleManager<AppRoleManager>()
            .AddEntityFrameworkStores<AppIdentityDbContext>();
        }
        public static void AddAppAuthentication(this IServiceCollection services, IConfiguration config)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidIssuer = Settings.ISSUER,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Settings.JWT_SECRET_KEY)),
                    ValidAudience = Settings.AUDIENCE,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(Settings.ACCESS_LIFETIME)
                };
            });
        }
        public static void UseAppCors(this IApplicationBuilder app)
        {
            app.UseCors(opt =>
            {
                opt.AllowAnyMethod();
                opt.AllowAnyHeader();
                opt.AllowAnyOrigin();
            });
        }
        public static void AddAppSwagger(this IServiceCollection services, IConfiguration config)
        {
            var version = config["Application:Version"];

            services.AddSwaggerGen(c =>
            {
                c.CustomSchemaIds(type => type.ToString());
                c.SwaggerDoc(version, new OpenApiInfo { Title = config["Application:Name"], Version = version });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter 'Bearer' [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            Array.Empty<string>()
                    }
                });
            });
        }
        public static void AddAppMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Data.Config.AutomapperProfiles.UserProfile());
                mc.AddProfile(new Data.Config.AutomapperProfiles.WorkspaceProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
        public static void AddFluentValidation(this IServiceCollection services)
        {
            services.AddFluentValidation(config =>
            {
                config.RegisterValidatorsFromAssemblyContaining<SignUp.RequestValidator>();
                config.RegisterValidatorsFromAssemblyContaining<SignIn.RequestValidation>();
            });
        }
        public static void UseAppSwagger(this IApplicationBuilder app, IConfiguration config)
        {
            var version = config["Application:Version"];

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{config["Application:Name"]} {version}"));
        }
    }
}
