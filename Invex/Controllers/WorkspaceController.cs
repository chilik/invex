﻿using Core.Constants;
using Core.Models;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Invex.Controllers
{
    [Route("api/workspaces")]
    [ApiController]
    public class WorkspaceController : ControllerBase
    {
        private readonly IWorkspaceService _service;
        public WorkspaceController(IWorkspaceService service)
        {
            _service = service;
        }
        
        [HttpGet]
        [Route("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetWorkspaceByIdAsync([FromRoute] Guid id)
        {
            try
            {
                var result = await _service.GetById(id);
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetWorkspacesAsync()
        {
            try
            {
                var result = await _service.GetAll();
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> CreateWorkspaceAsync(Workspace.Request request)
        {
            try
            {
                var result = await _service.Create(request);
                return StatusCode(StatusCodes.Status201Created, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpPut]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> UpdateWorkspaceAsync(Workspace.Request request)
        {
            try
            {
                var result = await _service.Update(request);
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> UpdateWorkspaceAsync([FromRoute] Guid id)
        {
            try
            {
                await _service.Delete(id);
                return StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }
    }
}
