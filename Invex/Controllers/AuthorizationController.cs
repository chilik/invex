﻿using Core.Constants;
using Core.Models;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Invex.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly IAuthorizationService _service;
        public AuthorizationController(IAuthorizationService service)
        {
            _service = service;
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn(SignIn.Request request)
        {
            try
            {
                var result = await _service.SignIn(request);
                return StatusCode(StatusCodes.Status200OK, result);
            } 
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpPost("sign-up")]
        public async Task<IActionResult> SignUp(SignUp.Request request)
        {
            try
            {
                var result = await _service.SignUp(request);
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh(Refresh.Request request)
        {
            try
            {
                var result = await _service.Refresh(request);
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }
    }
}
