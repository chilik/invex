﻿using Core.Constants;
using Core.Models;
using Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Invex.Controllers
{
    [Route("api/profile")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _service;
        public ProfileController(IProfileService service)
        {
            _service = service;
        }
        
        [HttpGet]
        [Route("{id}")]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> GetProfile([FromRoute] string id)
        {
            try
            {
                var result = await _service.GetById(id);
                return StatusCode(StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Error { Type = ErrorTypes.SERVER_ERROR, Message = ex.Message });
            }
        }
    }
}
