using Core.Constants;
using Core.Services.External;
using Core.Services.Implementations;
using Core.Services.Interfaces;
using Infrastructure.Seeder;
using Invex.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Invex
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddAppDbContext(Configuration);

            services.AddAppIdentity();

            services.AddAppAuthentication(Configuration);

            services.AddAppSwagger(Configuration);

            services.AddAppMapper();

            services.AddScoped<ITokenService, TokenService>();

            services.AddScoped<IAuthorizationService, AuthorizationService>();
            
            services.AddScoped<IProfileService, ProfileService>();

            services.AddScoped<IWorkspaceService, WorkspaceService>();

            services.AddFluentValidation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppRoleManager roleManager, AppUserManager userManager)
        {
            IdentitySeeder.SeedRoles(roleManager);
            IdentitySeeder.SeedUser(userManager, "root@mail.com", "root", "Password1", Roles.ADMIN);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseAppSwagger(Configuration);
            }

            app.UseAppCors();

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
